# SMSC

Пакет для отправки смс через апи [smsc.ru](http://smsc.ru)

## Установка настроек для доступа к smsc.ru

```javascript
import { SMSC } from 'meteor/smsc';
// установка настроек
SMSC.setSettings({
  login: 'логин',
  password: 'пароль',
  sender: 'отправитель',
});
```

## Отправка смс
```javascript
import { SMSC } from 'meteor/smsc';

// phone {string|array} телефон или массив телефонов
// message {string} сообщение
SMSC.send(phone, message)
  // удачная отправка 
  .then((r) => {
    console.log(r);
  })
  // ошибка
  .catch((e) => {
    console.log(e);
  })
```



 

