import { Meteor } from 'meteor/meteor';
import { HTTP } from 'meteor/http';

class SMS {
  /**
   * Ссылка на апи smsc.ru
   * @type {string}
   */
  url = 'http://smsc.ru/sys/send.php';

  /**
   * Логин
   * @type {string}
   */
  login = '';

  /**
   * Пароль
   * @type {string}
   */
  psw = '';

  /**
   * Отправить
   * @type {string}
   */
  sender = '';

  /**
   * Установка настроек
   * @param login {string} логин
   * @param password {string} пароль
   * @param sender {string} отправитель
   */
  setSettings({ login, password, sender }) {
    this.login = login;
    this.psw = password;
    this.sender = sender;
  }

  /**
   * Метод для отправки смс
   * @param phones {string|array} телефон или массив телефонов для отправки
   * @param mes сообщение для отправки
   * @returns {Promise}
   */
  async send(phones, mes) {
    if (Array.isArray(phones)) {
      phones = phones.join(',');
    }
    if (Meteor.isDevelopment) {
      console.log({ phones, mes });
      return Promise.resolve('Сообщение отправлено!');
    }

    const { login, psw, sender } = this;

    if (!login || !psw || !sender) {
      return Promise.reject(new Meteor.Error('Нет логина или пароль или отправителя смс центра!'));
    }

    return new Promise((resolve, reject) => {
      HTTP.get(this.url, {
        params: {
          login, psw, charset: 'utf-8', phones, mes, sender,
        },
      }, (e, r) => {
        if (e) {
          reject(new Meteor.Error(`Ошибка запроса по ${phones} ${mes}`));
        }
        resolve(`Ответ смс шлюза: ${r.content}`);
      });
    });
  }
}

/**
 * Инстанс для работы смс
 * @type {SMS}
 */
const SMSC = new SMS();

export { SMSC };
