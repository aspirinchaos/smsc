// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by smsc.js.
import { name as packageName } from "meteor/smsc";

// Write your tests here!
// Here is an example.
Tinytest.add('smsc - example', function (test) {
  test.equal(packageName, "smsc");
});
