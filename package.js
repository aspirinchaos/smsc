Package.describe({
  name: 'smsc',
  version: '0.1.0',
  summary: 'Meteor package for use smsc.ru',
  git: 'https://bitbucket.org/aspirinchaos/smsc.git',
  documentation: 'README.md',
});

Package.onUse((api) => {
  api.versionsFrom('1.5');
  api.use(['ecmascript', 'http']);
  api.mainModule('smsc.js', 'server');
});

Package.onTest((api) => {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('smsc');
  api.mainModule('smsc-tests.js');
});
